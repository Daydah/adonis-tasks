'use strict'
// remember to reference the Task model at the top
const Task = use('App/Models/Task')
//  remember to reference the Validator at the top
const { validate } = use('Validator')

class TaskController {
  async index ({ view }) {
    const tasks = await Task.all()
    return view.render('tasks.index', { tasks: tasks.toJSON() })
  }

  async store ({ request, response, session }) {
    // validate form input
    const validation = await validate(request.all(), {
      title: 'required|min:3|max:255'
    })

    // show error messages upon validation fail
    if (validation.fails()) {
      session.withErrors(validation.messages())
              .flashAll()

      return response.redirect('back')
    }

    // persist to database
    const task = new Task()
    task.title = request.input('title')
    task.desc = request.input('desc')
    await task.save()

    // Fash success message to session
    session.flash({ notification: 'Task and description added!' })

    return response.redirect('back')
  }

  async destroy ({ params, session, response }) {
    const task = await Task.find(params.id)
    await task.delete()

    // Fash success message to session
    session.flash({ notification: 'Task deleted!' })

    return response.redirect('back')
  }
}

module.exports = TaskController
