'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TasksSchema extends Schema {
  up () {
    this.table('tasks', (table) => {
      // alter table
      table.text('desc').nullable()
    })
  }

  down () {
    this.table('tasks', (table) => {
      // reverse alternations
      table.dropColumn('desc')
    })
  }
}

module.exports = TasksSchema
